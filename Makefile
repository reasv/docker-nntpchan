# 0xj9/nntpchan

export GOPATH := /go
export PATH := /go/bin:$(PATH)

all:
	build-dependencies git go bash libsodium-dev build-base coreutils
	apk add imagemagick libsodium sox ca-certificates ffmpeg
	git clone https://github.com/majestrate/nntpchan --depth=1 /srv/nntpchan
	cd /srv/nntpchan && ./build-js.sh
	git config --global http.https://gopkg.in.followRedirects true
	go get -v github.com/majestrate/srndv2
	cp $(GOPATH)/bin/srndv2 /bin/srndv2
	addgroup -g 1000 nntpchan
	adduser -D -G nntpchan -s /bin/sh -u 1000 -h /srv/nntpchan nntpchan
	cleanup /Makefile /go /srv/nntpchan/.git
