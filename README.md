0xj9/nntpchan(1) -- decentralized chan
======================================

see `etc/confd/templates/srnd.ini.tmpl` for configuration options. options are set via environment variables.

- nntp port: `1199`
- webui port: `18000`
- feeds file: `/srv/nntpchan/feeds.ini`
- feeds dir: `/srv/nntpchan/feeds.d`
- webroot dir: `/srv/nntpchan/webroot`
- articles dir: `/srv/nntpchan/articles`
