FROM 0xj9/minibase:latest
COPY Makefile Makefile
COPY etc /etc
RUN make
EXPOSE 18000 1199
WORKDIR /srv/nntpchan
ENTRYPOINT ["/bin/dumb-init", "start", "nntpchan", "/bin/srndv2"]
CMD ["run"]
